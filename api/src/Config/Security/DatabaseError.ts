/**
 * Object definition for "NotFoundError" error
 */
interface ErrorMsg extends Error {
  name: string,
  message: string,
  type: string,
  value: string | undefined
}

export default class ErrorManager {

  /**
   * Read the input error and return an adapted HTTP error code, based on the error name
   * @param error   the error message to read
   * @returns       adapted error code
   */
  public readError(error: Error): number {
    const Error400 = ["QueryFailedError", "BulkWriteError", "TokenExpiredError"]
    const Error401 = ["CredentialsError"]
    const Error403 = ["JsonWebTokenError", "NotBeforeError", "OwnershipError"]
    const Error404 = ["NotFoundError"]

    if(Error400.includes(error.name))
    {
      return 400
    }
    else if (Error401.includes(error.name))
    {
      return 401
    }
    else if (Error403.includes(error.name))
    {
      return 403
    }
    else if (Error404.includes(error.name))
    {
      return 404
    }
    return 500
  }

  
  /**
   * Generate a 404 error message with its type and name
   * @param type  the type of the ressource
   * @param name  the name of the ressource
   * @returns     a ErrorMsg object with all informations to generate the error code
   */
  public notFoundError(type: string, value: string): ErrorMsg {
    const error: ErrorMsg = {
      name: "NotFoundError",
      message: type + " " + value + " not found.",
      type: type,
      value: value
    }
    return error
  }

  /**
   * Generate a 400 error message with its type and name
   * @param type  the type of the ressource
   * @param value the name of the ressource
   * @returns     a ErrorMsg object with all informations to generate the error code
   */
  public badRequestError(type: string, value: string): ErrorMsg {
    const error: ErrorMsg = {
      name: "QueryFailedError",
      message: type.charAt(0).toUpperCase() + type.slice(1) + " '" + value + "' doesn't exists. You must create this " + type + " first.",
      type: type,
      value: value
    }
    return error
  }

  /**
   * Generate a 400 error message with its type and name
   * @param type  the type of the ressource
   * @param value the name of the ressource
   * @returns     a ErrorMsg object with all informations to generate the error code
   */
  public haveChildError(type: string, value: string): ErrorMsg {
    const error: ErrorMsg = {
      name: "QueryFailedError",
      message: type.charAt(0).toUpperCase() + type.slice(1) + " '" + value + "' is still associated to child ressources. Consider delete them first before deleting this " + type,
      type: type,
      value: value
    }
    return error
  }

  /**
   * Generate an 401 error when you try to login with wrong credentials
   * @param username  the username of the user
   * @returns         an ErrorMsg object with all informations to send the error
   */
  public wrongCredentialsError(username: string): ErrorMsg {
    const error: ErrorMsg = {
      name: "CredentialsError",
      message: "Invalid credentials for the user " + username,
      type: "user",
      value: username
    }
    return error
  }

  /**
   * Generate an 401 error when you forgot to provide the token
   * @returns         an ErrorMsg object with all informations to send the error
   */
  public missingToken(): ErrorMsg {
    const error: ErrorMsg = {
      name: "JsonWebTokenError",
      message: "Missing token for this operation. Please login first.",
      type: "JsonWebToken",
      value: undefined
    }
    return error
  }

  /**
   * Generate an 401 error when you try to manage ressources you are not owning
   * @param type      the type of the targeted ressource
   * @param value     the value of the targeted ressource
   * @returns         an ErrorMsg object with all informations to send the error
   */
  public ownershipError(type: string, value: string): ErrorMsg {
    const error: ErrorMsg = {
      name: "OwnershipError",
      message: "The target ressource doesn't belong to the token provided.",
      type: type,
      value: value
    }
    return error
  }

  /**
   * Generate an 401 error when you try to delete an user which is still owner of ressources
   * @param username  the username of the user
   * @returns         an ErrorMsg object with all informations to send the error
   */
  public stillOwnerError(username: string): ErrorMsg {
    const error: ErrorMsg = {
      name: "OwnershipError",
      message: "User is still associated to child ressources. Consider delete them first before removing this user.",
      type: "user",
      value: username
    }
    return error
  }
}
