import { ErrorManager } from "."
import { AnimeController, TypeController, WaifuController } from "../Controllers"
import { AnimeModel, TypeModel } from "../Models"

export default class RessourceChecker {
  /**
   * Check if a ressource exist before creating a sub ressource
   * @param type  the type of the ressource
   * @param name  the name of the ressource
   */
  private manager = new ErrorManager()

  public async isRessourceDefined(type: string, value: string): Promise<AnimeModel | TypeModel>  {
    try {
      if (type == "anime") {
        const controller = new AnimeController
        return await controller.retrieve_one_by_name(value)
      } else {
        const controller = new TypeController
        return await controller.retrieve_one_by_name(value)
      }
    } catch (error) {
      throw this.manager.badRequestError(type, value) as Error
    }
  }

  /**
   * Check if a ressource have children before deleting it
   * @param type  the type of the ressource
   * @param name  the name of the ressource
   */
   public async isRessourceHaveChild(type: string, value: string): Promise<void> {
    let children = null
    
    if (type == "anime") {
      const controller = new WaifuController
      children = await controller.retrieve_all_by_anime(value)
    } else {
      const controller = new AnimeController
      children = await controller.retrieve_all_by_type(value)
    }

    if(children.length > 0) {
      throw this.manager.haveChildError(type, value) as Error
    } 
  }
}