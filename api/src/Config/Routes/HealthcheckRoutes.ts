import { Router } from "express"

const router: Router = Router()

/**
 *  Healthcheck route
 *  @return     status
 */
router.get("/", (_, res) => { return res.status( 204 ).send() } )

export default router