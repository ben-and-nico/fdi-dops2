import express from "express"

import { UserController } from "../Controllers"
import { ErrorManager } from "../Security"


const router = express.Router()
const userController = new UserController()
const errorManager = new ErrorManager()


/**
 *  POST new user async route
 *  @return     status + response
 */
 router.post("/", async (req, res) => {
  try {
    const response = await userController.create(req)
    return res.status(201).send(response)
  } catch (error) {
    const code = errorManager.readError(error as Error)
    return res.status(code).send(error)
  }
})


/**
 *  DELETE one user by name async route
 *  @return     status + response
 */
router.delete("/:username", async (req, res) => {
  try {
    await userController.delete(req)
    return res.status(204).send()
  } catch (error) {
    const code = errorManager.readError(error as Error)
    return res.status(code).send(error)
  }
})


/**
 *  POST login async route
 *  @return     status + response
 */
 router.post("/login", async (req, res) => {
  try {
    const response = await userController.login(req)
    return res.status(200).send(response)
  } catch (error) {
    const code = errorManager.readError(error as Error)
    return res.status(code).send(error)
  }
})


export default router
