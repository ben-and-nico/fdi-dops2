import express from "express"
import { AnimeController } from "../Controllers"
import { ErrorManager } from "../Security"

const router = express.Router()
const animeController = new AnimeController()
const errorManager = new ErrorManager()


/**
 *  GET all anime async route
 *  @return     status + response
 */
router.get("/", async (_, res) => {
  try {
    const response = await animeController.retrieve_all()
    return res.status(200).send(response)
  } catch (error) {
    const code = errorManager.readError(error as Error)
    return res.status(code).send(error)
  }
})
  

/**
 *  GET one anime by waifu name async route
 *  @return     status + response
 */
router.get("/waifu/:name", async (req, res) => {
  try {
    const response = await animeController.retrieve_one_by_waifu(req.params.name)
    return res.status(200).send(response)
  } catch (error) {
    const code = errorManager.readError(error as Error)
    return res.status(code).send(error)
  }
})


/**
 *  POST new anime async route
 *  @return     status + response
 */
router.post("/", async (req, res) => {
  try {
    const response = await animeController.create(req)
    return res.status(201).send(response)
  } catch (error) {
    const code = errorManager.readError(error as Error)
    return res.status(code).send(error)
  }
})


/**
 *  GET one anime by name async route
 *  @return     status + response
 */
router.get("/:name", async (req, res) => {
  try {
    const response = await animeController.retrieve_one_by_name(req.params.name)
    return res.status(200).send(response)  
  } catch (error) {
    const code = errorManager.readError(error as Error)
    return res.status(code).send(error)
  }
})


/**
 *  PUT one anime by name async route
 *  @return     status + response
 */
router.put("/:name", async (req, res) => {
  try {
    const response = await animeController.update(req)
    return res.status(200).send(response)
  } catch (error) {
    const code = errorManager.readError(error as Error)
    return res.status(code).send(error)
  }
})


/**
 *  DELETE one anime by name async route
 *  @return     status + response
 */
router.delete("/:name", async (req, res) => {
  try {
    await animeController.delete(req)
    return res.status(204).send()
  } catch (error) {
    const code = errorManager.readError(error as Error)
    return res.status(code).send(error)
  }
})

export default router
