import {
  Entity,
  Column,
  ObjectID,
  ObjectIdColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Index
} from "typeorm"


/**
 *  Entity to describe Types in database
 */
@Entity("types")
export class TypeModel {

  @ObjectIdColumn()
  _id!: ObjectID

  @Index({ unique: true })
  @Column()
  name!: string

  @Column()
  image!: string

  @Column({
    type: "text"
  })
  description!: string

  @Column()
  owner!: string

  @CreateDateColumn()
  createdAt!: Date

  @UpdateDateColumn()
  updatedAt!: Date
}
