import winstonLogger from "./winston"
import morganLogger from "./morgan"

export { winstonLogger, morganLogger }