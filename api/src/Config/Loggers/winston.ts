import winston from "winston"

/**
 *  Define all different options for the winston configuration
 */
interface WinstonConfig {
  levels: winston.config.AbstractConfigSetLevels,
  colors: winston.config.AbstractConfigSetColors,
  date_format: string,
  isAllColors: boolean
}

/**
 *  Winston configuration with all parameters
 */
const config: WinstonConfig = {
  levels: {
    error: 1,
    warn: 2,
    info: 3,
    http: 4,
    debug: 5
  },
  colors: {
    error: "red",
    warn: "orange",
    info: "yellow",
    http: "green",
    debug: "white"
  },
  date_format: "YYYY-MM-DD / HH:mm:ss",
  isAllColors: true
}

/**
 *  Add our color scheme to the winston configuration
 */
winston.addColors(config.colors)

/**
 *  Customize the format of the logs (use colors + timestamps + format)
 */
const format = winston.format.combine(
  winston.format.timestamp({ format: config.date_format }),
  winston.format.colorize({ all: config.isAllColors }),
  winston.format.printf( info => {
    return ("[ " + info.timestamp + " ] - " + info.level + ": " + info.message)
  })
)

/**
 *  Allow winston to write to the default console all logs
 */
const transports = [
  new winston.transports.Console()
]

/**
 *  Logger instance to export
 */
const winstonLogger = winston.createLogger({
  level: "debug",
  levels: config.levels,
  format: format,
  transports,
})

export default winstonLogger