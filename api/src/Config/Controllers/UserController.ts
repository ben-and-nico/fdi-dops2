import { DeleteResult } from "typeorm"
import { Request } from "express"

import { UserModel } from "../Models"
import { AuthManager, ErrorManager } from "../Security"
import { tokenInformations } from "../Security/AuthManager"
import { 
  UserSchema,
  createUser,
  getOneUserByUsername,
  deleteOneUser
} from "../Repositories/UserRepo"


const auth = new AuthManager()
const dbError = new ErrorManager()

export class UserController {
  /**
   *  call the user repo to create a brand new user
   *  @param  body    format the sent body to a UserSchema
   *  @return         a promise with the UserModel created
   */
  public async create(
    request: Request
  ): Promise<UserModel> {
    const body: UserSchema = request.body
    body.password = auth.encryptPassword(body.password)
    return createUser( body )
  }

  /**
   *  call the user repo to find one user in database
   *  @param  request   username to find
   *  @return           a promise with the UserModel found
   */
  public async retrieve_one(
    request: Request
  ): Promise<UserModel> {
    return getOneUserByUsername(
      request.body.username
    )
  }

  /**
   *  call the user repo to delete one user in database
   *  @param  request   username to find for deletion
   *  @return           a promise with the DeletedResult
   */
  public async delete(
    request: Request<{username: string}>
  ): Promise<DeleteResult> {
    if(await auth.isStillOwner(request.params.username))
    {
      throw dbError.stillOwnerError(request.params.username) as Error
    }
    if(!await auth.isHimself(request.params.username, request.headers.authorization))
    {
      throw dbError.ownershipError("user", request.params.username)
    }
    return deleteOneUser(
      request.params.username
    )
  }

  /**
   *  Try to login someone based on credentials provided
   *  @param  body    format the sent body to a UserSchema
   *  @return         a promise with the JWT Token
   */
  public async login(
    request: Request
  ): Promise<tokenInformations> {
    const body: UserSchema = request.body
    const user = await this.retrieve_one(request)
    body.password = auth.encryptPassword(body.password)
    return auth.checkCredentials(body, user)
  }
}
