import { WaifuController } from "./WaifuController"
import { AnimeController } from "./AnimeController"
import { TypeController } from "./TypeController"
import { UserController } from "./UserController"
export { WaifuController, AnimeController, TypeController, UserController }
